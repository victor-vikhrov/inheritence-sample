package com.company;

import java.util.Arrays;

public class Main {

    static Client[] clients = new Client[3];
    static VipClient[] vipClients = new VipClient[3];

    public static void main(String[] args) {

        Arrays.sort(clients); // сортировка клиентов
        printArray(clients); // вывод клиентов в консоль

        System.out.println("\n*** VIP Клиенты ***\n");

        Arrays.sort(vipClients);
        printArray(vipClients);

    }

    private static void printArray(Client[] clients) {
        for (Client client : clients) {
            System.out.println(client.toString());
            System.out.println("=======================");
        }
    }

    static {
        Client client1 = new Client();
        client1.setName("John Smith");
        client1.setPhone("124-151-1251");
        client1.setId(10001);
        client1.setOrderIds(new String[] {"11242414", "14151515", "12515155"});
        clients[0] = client1;

        Client client2 = new Client();
        client2.setName("Sam Martin");
        client2.setPhone("612-462-1515");
        client2.setId(10002);
        client2.setOrderIds(new String[] {"36261231"});
        clients[1] = client2;

        Client client3 = new Client();
        client3.setName("Adam Williams");
        client3.setPhone("824-124-0119");
        client3.setId(10003);
        client3.setOrderIds(new String[] {});
        clients[2] = client3;

        VipClient vipClient1 = new VipClient();
        vipClient1.setName("John Travolta");
        vipClient1.setPhone("47-905-025483");
        vipClient1.setId(10004);
        vipClient1.setRating(7100);
        vipClient1.setOrderIds(new String[] {"12410101", "09184911"});
        vipClients[0] = vipClient1;

        VipClient vipClient2 = new VipClient();
        vipClient2.setName("Samuel L. Jackson");
        vipClient2.setPhone("55-42-06726");
        vipClient2.setId(10005);
        vipClient2.setRating(12500);
        vipClient2.setOrderIds(new String[] {"91810101", "75192211"});
        vipClients[1] = vipClient2;

        VipClient vipClient3 = new VipClient();
        vipClient3.setName("Bruce Willis");
        vipClient3.setPhone("870-442-04279");
        vipClient3.setId(10006);
        vipClient3.setRating(9000);
        vipClient3.setOrderIds(new String[] {});
        vipClients[2] = vipClient3;

    }


}
