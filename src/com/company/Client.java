package com.company;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by victor on 15.11.17.
 */

public class Client implements Comparable<Client> {

    // private - доступ только внутри класса
    // protected - доступ внутри класса и в наследуемых классах
    // package (по умолчанию) - доступ на уровне пакета
    // public - доступно всем

    private static final double DEFAULT_DISCOUNT = 0.05;

    protected long id;
    protected String name;
    protected String phone;

    protected String[] orderIds;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String[] getOrderIds() {
        return orderIds;
    }

    public void setOrderIds(String[] orderIds) {
        this.orderIds = orderIds;
    }

    /*
    * метод getDiscount возвращает скидку для клиента
    */
    public double getDiscount() {
        if (orderIds.length == 0) {
            return DEFAULT_DISCOUNT;
        } else {
            return 0.0;
        }
    }

    /*
    * метод compareTo сравнивает клиентов по имени
    */
    public int compareTo(Client client) {
        return name.compareTo(client.getName());
    }

    /*
    * метод equals сравнивает клиентов по id
    */
    @Override
    public boolean equals(Object obj) {
        Client client = (Client) obj;
        if (this.id == client.id) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "Клиент: " + name + " (тел. " + phone + ")" + "\n"
                + "Заказы: " + Arrays.asList(orderIds).toString();
    }


}
